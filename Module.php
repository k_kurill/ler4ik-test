<?php
/**
 * Created by PhpStorm.
 * User: valer
 * Date: 09.02.2017
 * Time: 19:24
 */

namespace ler4ik\food;

use yii\base\Module as BaseModule;
class Module extends BaseModule
{
    //роль пользователя для доступа администрирования
    public $adminPermission = 'manage_food';
    
    //максимальное число ингрединтов для поиска
    public $maxSearchIngredient = 5;
    
    //минимальное число ингредиентов для поиска
    public $minSearchIngredient = 2;


}