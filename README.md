food
====
Тестовое задание: модуль "Food"

Установка
---------
Добавить репозиторий в `composer.json`

```
"repositories": [{
        "type": "git",
        "url": "https://k_kurill@bitbucket.org/k_kurill/ler4ik-test.git"
    }
],
```

Добавить расширение в секцию require

```
"ler4ik/yii2-food": "*"
```

Выполнить миграции
```
yii migrate/up --migrationPath=vendor/ler4ik/yii2-food/migrations
```

Подключить модуль в конфигурационном файле приложения

```
...
'modules' => [
    ...
    'food' => [
        'class' => 'ler4ik\food\Module',
        'adminPermission' => 'manage_food',
        'maxSearchIngredient' => 5,
        'minSearchIngredient' => 2
    ],
    ...
],
...
```

Использование
-------------

Администрирование ингредиентов:
/food/ingredient/index

Администрирование блюд:
/food/dish/index