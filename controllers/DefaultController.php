<?php
namespace ler4ik\food\controllers;

use ler4ik\food\models\DishSearch;
use ler4ik\food\models\Ingredient;
use yii\web\Controller;
use yii\db\Query;
use Yii;


class DefaultController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new DishSearch();
        $searchModel->minSearchCount = $this->module->minSearchIngredient;
        $dataProvider = $searchModel->frontendSearch(Yii::$app->request->post());
        return $this->render('index',  [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * возвращает список активных ингредиентов
     * @param string $q запрос
     * @param integer $id
     * @return array
     */
    public function actionIngredientList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, name AS text')
                ->from(Ingredient::tableName())
                ->where(['like', 'name', $q])
                ->andWhere(['active' => 1])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Ingredient::findOne($id)->name];
        }
        return $out;
    }
}
