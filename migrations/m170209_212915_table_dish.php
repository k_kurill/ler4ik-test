<?php

use yii\db\Migration;

class m170209_212915_table_dish extends Migration
{
    public function safeUp()
    {
        $this->createTable('dish', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('dish');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
