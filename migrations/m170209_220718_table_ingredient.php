<?php

use yii\db\Migration;

class m170209_220718_table_ingredient extends Migration
{
    public function safeUp()
    {
        $this->createTable('ingredient', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'active' => $this->boolean()
        ]);
        $this->createIndex('ACTIVE', 'ingredient', 'active', false);

    }

    public function safeDown()
    {
        $this->dropTable('ingredient');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
