<?php

use yii\db\Migration;

class m170210_095942_table_ingredient_dish extends Migration
{
    public function safeUp()
    {
        $this->createTable('ingredient_dish',[
            'ingredient_id' => $this->integer(),
            'dish_id' => $this->integer()

        ]);
        $this->addPrimaryKey('INGR_DISH_PK', 'ingredient_dish', ['ingredient_id', 'dish_id']);
        $this->addForeignKey('INGREDIENT_FK', 'ingredient_dish', 'ingredient_id', 'ingredient', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('DISH_FK', 'ingredient_dish', 'dish_id', 'dish', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('ingredient_dish');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
