<?php

use yii\db\Migration;

class m170212_154758_add_active_table_dish extends Migration
{
    public function safeUp()
    {
        $this->addColumn('dish', 'active', $this->boolean());

    }

    public function safeDown()
    {
        $this->dropColumn('dish', 'active');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
