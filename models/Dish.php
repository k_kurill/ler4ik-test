<?php

namespace ler4ik\food\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dish".
 *
 * @property int $id
 * @property string $name
 * @property int $active
 *
 * @property IngredientDish[] $ingredientDishes
 */
class Dish extends \yii\db\ActiveRecord
{
    private $_ingredientsArray = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['ingredientsArray'], 'safe'],
            ['ingredientsArray', 'required'],
            [['active'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ingredientsArray' => 'Ingredients',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])->viaTable('ingredient_dish', ['dish_id' => 'id']);
    }

    public function getIngredientsArray()
    {
        return ArrayHelper::getColumn($this->ingredients, 'id');
    }

    public function getIngredientsNames()
    {
        return ArrayHelper::getColumn($this->ingredients, 'name');
    }

    public function setIngredientsArray($ingredients)
    {
        $this->_ingredientsArray = $ingredients;
    }
    
    public function beforeSave($insert) {
        $query = Ingredient::find();
        $query->andWhere(['in', 'id', $this->_ingredientsArray]);
        $query->andWhere(['active'=>0]);
        if($query->exists()){
            $this->active = 0;
        }else{
            $this->active = 1;
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        //удаляем все текущие ингредиенты
        Yii::$app->db->createCommand()->delete('ingredient_dish', 
                ['dish_id' => $this->id])->execute();
        
        if(!$this->_ingredientsArray){
            return;
        }
        
        //формируем массив ингредиентов из формы
        $data = [];
        foreach ($this->_ingredientsArray as $ingredient){
            $data[] = [$this->id, $ingredient];
        }
        //сохраняем новые ингредиенты
        Yii::$app->db->createCommand()->batchInsert('ingredient_dish', 
                ['dish_id', 'ingredient_id']
                , $data)->execute();
    }
}
