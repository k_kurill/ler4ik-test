<?php

namespace ler4ik\food\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ler4ik\food\models\Dish;
use yii\db\Query;
use yii\db\Expression;

/**
 * DishSearch represents the model behind the search form of `ler4ik\food\models\Dish`.
 */
class DishSearch extends Dish
{
    public $ingredientIds = [];
    //минимальное количество совпадений для поиска блюд
    public $minSearchCount = 2;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['active'], 'integer'],
            [['name'], 'safe'],
            ['ingredientIds', 'each', 'rule' => ['integer']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dish::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
    public function frontendSearch($params)
    {
        $query = Dish::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //убираем пустые значения
        $ingredientIds = array_diff($this->ingredientIds,['']);
        if ($ingredientIds && count($ingredientIds) < $this->minSearchCount){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $this->addError('ingredientIds', 'Выберите больше ингредиентов');
            return $dataProvider;
        }

        //субзапрос из таблицы связи для полного совпадения
        $subQuery = new Query();
        $subQuery->from('ingredient_dish');
        $subQuery->select('dish_id');
        $subQuery->andWhere(['in','ingredient_id', $ingredientIds]);
        $subQuery->groupBy('dish_id');
        $subQuery->having(new Expression('count(dish_id)=:count'), ['count' => count($ingredientIds)]);

        //если есть полное совпадение выводим его
        if ($subQuery->count()){
            $query->andWhere(['in', 'id', $subQuery]);
            return $dataProvider;
        }
        //ищем частичные совпадения
        $query->select('dish.*, count(dish_id) as ingr_count');
        $query->leftJoin('ingredient_dish', ['dish.id'=>new Expression('ingredient_dish.dish_id')]);
        $query->groupBy('dish_id');
        $query->having(new Expression('ingr_count>=:count'), ['count' => $this->minSearchCount]);
        $query->orderBy(['ingr_count' => SORT_DESC]);
        $query->andWhere(['in','ingredient_id', $ingredientIds]);
        return $dataProvider;
    }
}
