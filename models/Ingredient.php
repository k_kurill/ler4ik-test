<?php

namespace ler4ik\food\models;

use Yii;
use yii\db\Query;
use yii\db\Expression;
/**
 * This is the model class for table "ingredient".
 *
 * @property int $id
 * @property string $name
 * @property int $active
 *
 * @property Dish[] $dishes
 */
class Ingredient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['active'], 'default', 'value' => '1'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishes()
    {
        return $this->hasMany(Dish::className(), ['dish.id' => 'dish_id'])
                ->viaTable('ingredient_dish', ['ingredient_id' => 'id']);
    }
    
    /**
     * Делает неактивными блюда, у которых неактивен один из ингредиентов
     */
    public static function checkDishActivity()
    {
        //Делаем все блюда активными
        Dish::updateAll(['active' => 1]);
        
        //Ищем блюда с неактивными ингредиентами
        $subQuery = new Query();
        $subQuery->from('ingredient_dish');
        $subQuery->select('dish_id');
        $subQuery->innerJoin(
                'ingredient', 
                ['ingredient.id' => new Expression('ingredient_id')]
        );
        $subQuery->andWhere(['ingredient.active' => 0]);
        
        //Делаем неактивными блюда, у которых есть неактивный ингредиент
        Dish::updateAll(['active' => 0], ['in', 'dish.id', $subQuery]);
    }
    
    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->checkDishActivity();
    }
}
