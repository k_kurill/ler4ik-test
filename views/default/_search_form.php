<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use ler4ik\food\models\Ingredient;
?>

<?php
$form = ActiveForm::begin() ?>
<?php echo $form->errorSummary([$model])?>
<div class="row">
<?php for ($i=0; $i<$this->context->module->maxSearchIngredient; $i++):?>
<?php $value = ArrayHelper::getValue($model->ingredientIds, $i, null);?>
        <div class="form-group col-md-2">
            <?=Select2::widget([
                'options' => ['placeholder' => 'Search for a ingredient ...', 'class' => 'col-md'],
                'name' => 'DishSearch[ingredientIds][]',
                'initValueText' => Ingredient::findOne($value) ? Ingredient::findOne($value)->name : '',
                'value' => $value,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'multiple' => false,
                    'class' => '100px',
                    'ajax' => [
                        'url' => Url::to(['default/ingredient-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ]);?>
        </div>
<?php endfor;?>
<div class="form-group">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>

