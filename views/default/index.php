<?php
use yii\widgets\ListView;
use ler4ik\food\models\DishSearch;
use yii\data\ActiveDataProvider;
use app\modules\food\models\Dish;
$this->title = 'Dish';
?>
<?= $this->render('_search_form', ['model' => $searchModel]);?>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_list_item',
]);
