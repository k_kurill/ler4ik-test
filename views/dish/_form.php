<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model ler4ik\food\models\Dish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'ingredientsArray')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Search for a ingredient ...'],
        'initValueText' => $model->ingredientsNames,
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'multiple' => true,
            'ajax' => [
                'url' => Url::to(['dish/ingredient-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
        ],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
